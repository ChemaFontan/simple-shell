package main.commands;

import java.util.HashMap;

import main.ShellController;

public class StatisticsCommand extends Command {

	@Override
	public String getResults(ShellController myShell) {
		
		String results = "";
		this.succeed++;
		HashMap<String, Command> StatisticMap = myShell.getStatistics();
				
		for (String s : StatisticMap.keySet()) {
			if (!s.equals("exit")) results += String.format("%s:%d:%d%n",s
					, StatisticMap.get(s).getSucceed(), StatisticMap.get(s).getFailed());
		}
		
		return results;
	}

}
