package main.commands;

import java.io.File;
import java.util.LinkedList;

import main.ShellController;

public class DirCommand extends Command {

	@Override
	public String getResults(ShellController myShell) {

		return this.getFormattedList(myShell.getCurrentDir());
	}

	private File[] getCompleteList(String path) {

		File directory = new File(path);
		File[] listFiles = directory.listFiles();
		return (listFiles == null) ? new File[0] : listFiles;

	}

	protected LinkedList<File> getListofFiles(String path) {

		File[] arrayofFiles = this.getCompleteList(path);
		LinkedList<File> listofFiles = new LinkedList<File>();

		for (File f : arrayofFiles) if (f.isFile())	listofFiles.add(f);
			
		return listofFiles;

	}

	protected LinkedList<File> getListofDir(String path) {

		File[] arrayofFiles = this.getCompleteList(path);
		LinkedList<File> listofFiles = new LinkedList<File>();

		for (File f : arrayofFiles) 
			if (f.isDirectory()) listofFiles.add(f);
		
		this.succeed++;
		return listofFiles;

	}

	private String getFormattedList(String path) {

		StringBuffer bufferofFiles = new StringBuffer();
		
		this.getListofDir(path).forEach(f -> bufferofFiles.append(String.format("%-7s%s%n", "DIR",f.getName())));
		this.getListofFiles(path).forEach(f -> bufferofFiles.append(String.format("%-7s%s%n", "FILE",f.getName())));
				
		return bufferofFiles.toString();

	}

}
