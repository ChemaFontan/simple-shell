package main.commands;

import main.ShellController;

public class ExitCommand extends Command {

	@Override
	public String getResults(ShellController myShell) {
		
		return "Bye!";
	}

}
