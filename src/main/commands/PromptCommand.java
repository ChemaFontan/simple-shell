package main.commands;

import java.util.HashMap;

import main.ShellController;

public class PromptCommand extends Command{
	
	private final HashMap<String,String> listofParameters = new HashMap<String, String>();
	
	public PromptCommand () {
		listofParameters.put("reset", "$");
	}

	public String getResults(ShellController myShell) {
		
		listofParameters.put("$cwd", myShell.getCurrentDir());

		String parameter = "";
				
		for (String p : myShell.getParameters()) {
			parameter += p;
		}
		
		parameter = parameter.toLowerCase();
		
		if (listofParameters.containsKey(parameter)) {
			myShell.setPrompt(listofParameters.get(parameter));
		}else {
			myShell.setPrompt(parameter);
		}
		this.succeed++;
		return "Prompt Changed!";
	}

}
