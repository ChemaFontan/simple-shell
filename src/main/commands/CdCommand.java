package main.commands;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import main.ShellController;

public class CdCommand extends Command {

	@Override
	public String getResults(ShellController myShell) {
		
		String parameter;
		LinkedList<String> params = myShell.getParameters();
		
		parameter = (params.size() == 1) ? params.getFirst() : ""; 
		
		File directory = new File (myShell.getCurrentDir());
		File[] listofFiles = directory.listFiles();
		boolean dirExists = parameter.equals("..");
		
		for (File f : listofFiles) {
			if (f.getName().compareTo(parameter) == 0) {
				dirExists = true;
			}
		}
		
		if (dirExists) {
			directory = new File (myShell.getCurrentDir() + myShell.slash + parameter);
			try {
				if (myShell.getPrompt().equals(myShell.getCurrentDir())) {
					myShell.setPrompt(directory.getCanonicalPath());
				}
				myShell.setCurrentDir(directory.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.succeed++;
			return String.format("Working directory changed to %s", myShell.getCurrentDir());
		}		
		this.failed++;
		return "Wrong directory";
	}

}
