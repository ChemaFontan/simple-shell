package main.commands;

import main.ShellController;

public abstract class Command {
	
	int succeed, failed;
	
	public abstract String getResults (ShellController myShell);
	
	public 	int getSucceed () {
		return succeed;
	}
	
	int getFailed () {
		return failed;
	}
	
}
