package main.commands;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import main.ShellController;

public class TreeCommand extends Command {

	@Override
	public String getResults(ShellController myShell) {

		File rootDir = new File(myShell.getCurrentDir());
		String result = String.format("%s%n%s", rootDir.getName(), getandPrintRecursively(rootDir, 0));
		
		return result;
	}

	private String getandPrintRecursively(File f, int level) {

		DirCommand dir = new DirCommand();
		String result = "";
		LinkedList<File> mainListofFiles;

		try {
			mainListofFiles = dir.getListofDir(f.getCanonicalPath());
		} catch (IOException e) {
			mainListofFiles = null;
			e.printStackTrace();
		}

		for (File file : mainListofFiles) {

			for (int i = 0; i < (level); i++) {
				result += " ";
			}
			result += String.format("|-%s%n%s", file.getName(), getandPrintRecursively(file, level + 1));
		}
		return result;
	}

}