package main;

import java.util.Scanner;

public class MyShell {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner (System.in);
		String result = "";
		ShellController mainShellVars = new ShellController();
		
		while (!result.equals( "Bye!")) {
			System.out.print(String.format("%s>", mainShellVars.getPrompt()));
			result = mainShellVars.exec(userInput.nextLine());
			System.out.println(result);
		}
		userInput.close();
	}

}
