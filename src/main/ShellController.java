package main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import main.commands.*;



public class ShellController {
	
	private Command mainCommand;
	private LinkedList<String> parameters;
	private String prompt = "$";
	private String currentDir;
	public final String slash = System.getProperty("os.name").toLowerCase().equals("win") ? "\\" : "/";
	private final HashMap<String, Command> listofCommands = new HashMap <String, Command>();
	
	public ShellController () {
		
		listofCommands.put("dir", new DirCommand());
		listofCommands.put("exit", new ExitCommand());
		listofCommands.put("prompt", new PromptCommand());
		listofCommands.put("cd", new CdCommand());
		listofCommands.put("tree", new TreeCommand());
		listofCommands.put("statistics", new StatisticsCommand());
		
		File getPath = new File(".");
				
		try {
			currentDir = getPath.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Command getMainCommand() {		
		return mainCommand;
	}

	public LinkedList<String> getParameters() {
		return parameters;
	}
	public void setParameters(String command) {
		
		parameters = new LinkedList<String>();
		String[] params = command.split(" ");
		for (String s : params) parameters.add(s);
		String tempCommand = parameters.getFirst();
				
		mainCommand = listofCommands.containsKey(tempCommand) ? listofCommands.get(tempCommand) : null;
		parameters.removeFirst();
				
	}
	public String getCurrentDir() {
		return currentDir;
	}
	public void setCurrentDir(String currentDir) {
		this.currentDir = currentDir;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	
	public void setPromptasDir () {
		this.prompt = this.currentDir;
	}
	
	public String exec (String command) {
		
		setParameters (command);
		mainCommand = getMainCommand();
		
		return (mainCommand !=null) ? mainCommand.getResults(this) : "wrong command";
	}
	
	public HashMap<String, Command> getStatistics () {
		return listofCommands;
	}
	
}
