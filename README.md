# Simple Shell in Java

This project was created as a practice. It is a simple command line that implements the following DOS commands: DIR, CD, TREE and PROMPT. In addition it implements the command STATISTICS that shows the statistics for each command.

## Built With

* Eclipse Oxygen

## Authors

* **Jose Maria Fontan Frojan**

## License

This Project is not licensed. The author allows the copy, modification and share of this code
